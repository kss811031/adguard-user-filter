! Title: 개인 애드가드 필터
! Description: 대한민국 웹사이트 용 개인 필터 입니다.
! Version: 2025.0301.2
! Expires: 7 day
! Homepage: https://gitlab.com/kss811031/adguard-user-filter
! License: 
!


!#include antiadblock.txt


!#if (adguard_app_android || adguard_app_ios)
!#include mobile/antiadblock.txt

!#else
!#include desktop/antiadblock.txt

!#endif
