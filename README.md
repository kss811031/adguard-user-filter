# Adguard User Filter

애드가드 용 필터 입니다. <br/>
<br />

# How to use
<b>아래 링크를 복사한 후 광고차단 프로그램에 붙여넣으세요. </b>
<br/><br/>

**PC용**:
```
https://gitlab.com/kss811031/adguard-user-filter/-/raw/main/filter-AdGuard.txt
```
<br/>

**Mobile용**:
```
https://gitlab.com/kss811031/adguard-user-filter/-/raw/main/filter-AdGuard.txt
```
<br/>
